## Setting the versions

The versions are contained in `/src/version` :

```
[assembly: AssemblyVersion("3.1.1")]
nuget:3.1.1-test
```

The line beginning  `[assembly:` is used to version the javascript projects.
The `nuget:` line is the version that will be applied to the c# nuget packages created by the package step.

The `SetVersion` tool will read the `nuget:` line from this file and apply it to the projects referenced by the toplevel VS Solution file.
It will also apply the version to any `.nuspec` files.

**In order for this to be a success, the version number in any of the files that needs to be replaced with the version number from `/src/version` MUST be `0.0.0`**

### Setversion 
Setversion is provided as a Windows and Linux exe. 

Source repo: https://gitlab.com/monex.mark/setversion


Usage:

```PS C:\Work\FLOODNEW> .\tools\windows\SetVersion.exe ?
Usage   : SetVersion [versionfile] [solution File] ([additional nuspec file])
Example : SetVersion ./src/versionfile ./src/Floodgate.sln
        : SetVersion ./src/versionfile ./src/Floodgate.sln ./src/TradeStation.SDK.Floodgate.nuspec
```

Setversion will validate that the nuget packages created all have the same version number, and will warn if no `0.0.0` placeholders have been found.