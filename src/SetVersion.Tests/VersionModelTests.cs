using NUnit.Framework;
using Shouldly;

namespace SetVersion.Tests
{
    public class VersionModelTests
    {
        private const string TEST_NUM_1 = "1.2.3";
        private const string TEST_NUM_2 = "12.34.567";
        private const string TEST_NUM_POSTFIX_1 = "1.2.3-alpha";
        private const string TEST_NUM_POSTFIX_2 = "1.23.456-alpha";

        [Test]
        public void Numeric1_Valid_ParsesOK()
        {
            // arrange
            // act
            var result = VersionModel.FromString(TEST_NUM_1);

            // assert
            result.Major.ShouldBe(1);
            result.Minor.ShouldBe(2);
            result.Patch.ShouldBe(3);
            result.Postfix.ShouldBeEmpty();
        }

        [Test]
        public void Numeric2_Valid_ParsesOK()
        {
            // arrange
            // act
            var result = VersionModel.FromString(TEST_NUM_2);

            // assert
            result.Major.ShouldBe(12);
            result.Minor.ShouldBe(34);
            result.Patch.ShouldBe(567);
            result.Postfix.ShouldBeEmpty();
        }

        [Test]
        public void NumericWithPostfix1_Valid_ParsesOK()
        {
            // arrange
            // act
            var result = VersionModel.FromString(TEST_NUM_POSTFIX_1);

            // assert
            result.Major.ShouldBe(1);
            result.Minor.ShouldBe(2);
            result.Patch.ShouldBe(3);
            result.Postfix.ShouldBe("alpha");
        }

        [Test]
        public void NumericWithPostfix2_Valid_ParsesOK()
        {
            // arrange
            // act
            var result = VersionModel.FromString(TEST_NUM_POSTFIX_2);

            // assert
            result.Major.ShouldBe(1);
            result.Minor.ShouldBe(23);
            result.Patch.ShouldBe(456);
            result.Postfix.ShouldBe("alpha");
        }
    }
}