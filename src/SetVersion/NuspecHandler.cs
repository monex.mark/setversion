﻿using ByteDev.DotNet.Solution;
using System;
using System.Collections.Generic;
using System.IO;
using static System.Console;

namespace SetVersion
{
    internal static class NuspecHandler
    {
        private const string VERSION_PLACEHOLDER_ATTRIBUTE = "\"0.0.0\"";
        private const string VERSION_PLACEHOLDER_XML = "<version>0.0.0</version>";
        private const string VERSION_PLACEHOLDER_XML_REPLACE = "<version>{0}</version>";

        internal static int SetVersionInNuspecFiles(VersionModel version, IReadOnlyList<DotNetSolutionProject> projects, Options options)
        {
            try
            {
                foreach (var project in projects)
                    foreach (var nuspecFileLocation in Directory.GetFiles(Path.GetDirectoryName(Path.Combine(options.SolutionRoot, project.Path)), "*.nuspec"))
                        SetVersionInNuspecFile(version, nuspecFileLocation);

                if (options.HasExtraNuget)
                    SetVersionInNuspecFile(version, options.ExtraNuspecFileLocation);

                return Program.EXIT_SUCCESS;
            }
            catch (Exception ex)
            {
                WriteLine(ex.Message);
                return Program.EXIT_FAILURE;
            }
        }

        private static void SetVersionInNuspecFile(VersionModel version, string nuspecFileLocation)
        {
            try
            {
                WriteLine($"Setting version to {version} in:");
                WriteLine($"\t> {nuspecFileLocation}");

                var fileText = File.ReadAllText(nuspecFileLocation);

                if (PlaceholderExists(fileText) == false)
                {
                    var colour = ForegroundColor;
                    ForegroundColor = ConsoleColor.Yellow;
                    WriteLine("INFO: No placeholder value [ 0.0.0 ] found!");
                    ForegroundColor = colour;
                }

                var replaced = fileText.Replace(VERSION_PLACEHOLDER_XML, string.Format(VERSION_PLACEHOLDER_XML_REPLACE, version));
                replaced = replaced.Replace(VERSION_PLACEHOLDER_ATTRIBUTE, $"\"{version}\"");
                File.WriteAllText(nuspecFileLocation, replaced);
            }
            catch (Exception ex)
            {
                WriteLine($"FAIL: {ex.Message} in:");
                WriteLine($"\t> {nuspecFileLocation}");
            }
        }

        private static bool PlaceholderExists(string fileText)
            => fileText.Contains(VERSION_PLACEHOLDER_ATTRIBUTE) || fileText.Contains(VERSION_PLACEHOLDER_XML);
    }
}
