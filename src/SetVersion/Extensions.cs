﻿using System.Xml.Linq;

namespace SetVersion
{
    internal static class Extensions
    {
        /// <remarks>
        /// Source: http://stackoverflow.com/a/14892813/1636276
        /// </remarks>
        internal static XElement GetOrCreateElement(this XContainer container, string name)
        {
            var element = container.Element(name);
            if (element != null) return element;
            element = new XElement(name);
            container.Add(element);
            return element;
        }
    }
}