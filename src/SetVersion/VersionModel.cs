﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace SetVersion
{
    public class VersionModel
    {
        public int Major { get; }
        public int Minor { get; }
        public int Patch { get; }
        public string Postfix { get; }

        private VersionModel(int major, int minor, int patch, string postfix)
        {
            Major = major;
            Minor = minor;
            Patch = patch;
            Postfix = postfix;
        }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Postfix))
                return $"{Major}.{Minor}.{Patch}";
            else
                return $"{Major}.{Minor}.{Patch}-{Postfix}";
        }


        private static readonly Regex NugetRegex = new Regex(@"(\d+)?\.(\d+)?\.(\d+)?\-{0,1}(\w+){0,1}", RegexOptions.Compiled | RegexOptions.Singleline);

        public static VersionModel FromString(string versionString)
        {
            var items = NugetRegex.Split(versionString)
                .Where(i => string.IsNullOrEmpty(i) == false)
                .ToList();

            if (items.Count == 3)
                return new VersionModel(Convert.ToInt32(items[0]), Convert.ToInt32(items[1]), Convert.ToInt32(items[2]), string.Empty);
            else
                return new VersionModel(Convert.ToInt32(items[0]), Convert.ToInt32(items[1]), Convert.ToInt32(items[2]), items[3]);
        }

        internal static bool IsValid(string versionString) 
            => (string.IsNullOrEmpty(versionString) == false && NugetRegex.IsMatch(versionString) );
    }
}
