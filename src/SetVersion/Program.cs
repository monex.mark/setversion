﻿using System;
using System.IO;
using System.Linq;
using static System.Console;

namespace SetVersion
{
    internal static class Program
    {
        internal const int EXIT_SUCCESS = 0;
        internal const int EXIT_FAILURE = 1;

        internal static int Main(params string[] args)
        {
            if (args.Any() && args[0] == Options.VALIDATE_FLAG)
                return ValidateVersion(args);
            else
                return SetVersion(args);
        }

        private static int SetVersion(string[] args)
        {
            var (success, options) = Options.TryCreate(args);
            if (success == false)
            {
                Usage.PrintUsage();
                return ExitCode(EXIT_FAILURE);
            }

            var versionString = VersionFileReader.GetVersionString(options.VersionFileLocation);
            if (VersionModel.IsValid(versionString) == false)
            {
                WriteLine($"Please provide a version in the form of nuget:1.2.3 or nuget:1.2.3-alpha in the text file! [{versionString}]");
                return ExitCode(EXIT_FAILURE);
            }

            var version = VersionModel.FromString(versionString);
            WriteLine($"Setting versions to: {version}");

            var projects = SolutionParser.GetAllCsprojFiles(options.SolutionFileLocation, ExcludeTestProjects);

            foreach (var projFile in projects)
                WriteLine($"Found:\t{projFile.Name}");

            if (CsprojHandler.SetVersionsInCsprojFiles(version, projects, options.SolutionRoot) != EXIT_SUCCESS)
                return ExitCode(EXIT_FAILURE);

            if (NuspecHandler.SetVersionInNuspecFiles(version, projects, options) != EXIT_SUCCESS)
                return ExitCode(EXIT_FAILURE);

            return ExitCode(EXIT_SUCCESS);
        }

        private static int ValidateVersion(string[] args)
        {
            var (success, options) = Options.TryCreate(args);
            if (success == false)
            {
                Usage.PrintUsageValidate();
                return ExitCode(EXIT_FAILURE);
            }

            var versionString = VersionFileReader.GetVersionString(options.VersionFileLocation);
            if (VersionModel.IsValid(versionString) == false)
            {
                WriteLine($"Please provide a version in the form of nuget:1.2.3 or nuget:1.2.3-alpha in the text file! [{versionString}]");
                return ExitCode(EXIT_FAILURE);
            }

            var version = VersionModel.FromString(versionString);
            WriteLine($"Read version : {version}");

            // get packages 
            var nugetPackages = Directory.GetFiles(options.PackagesDirectory, "*.nupkg");
            var unexpectedVersions = nugetPackages.Where(p => p.EndsWith($".{version}.nupkg") == false).ToList();
            foreach (var unexpectedFileName in unexpectedVersions)
            {
                var colour = ForegroundColor;
                ForegroundColor = ConsoleColor.Red;
                WriteLine($"WARN: version mismatch in {unexpectedFileName}");
                ForegroundColor = colour;
            }

            if (unexpectedVersions.Any())
                return ExitCode(EXIT_FAILURE);
            else
                return ExitCode(EXIT_SUCCESS);
        }



        private static bool ExcludeTestProjects(string projectName)
            => projectName.ToLowerInvariant().EndsWith(".test") || projectName.ToLowerInvariant().EndsWith(".tests");

        private static int ExitCode(int code)
        {
            Environment.Exit(code);
            return code;
        }

    }

}
