﻿using ByteDev.DotNet.Solution;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using static System.Console;

namespace SetVersion
{
    internal static class CsprojHandler
    {
        internal static int SetVersionsInCsprojFiles(VersionModel version, IReadOnlyList<DotNetSolutionProject> csProjFiles, string solutionRoot)
        {
            var fileLocations = csProjFiles.Select(p => Path.Combine(solutionRoot, p.Path)).ToList();

            if (!CheckCsprojFiles(fileLocations))
                return Program.EXIT_FAILURE;

            foreach (var csprojFile in fileLocations)
                SetVersion(version.ToString(), csprojFile);

            PrintSuccessString(version.ToString(), fileLocations);
            return Program.EXIT_SUCCESS;
        }

        private static bool CheckCsprojFiles(IReadOnlyList<string> csprojFiles)
        {
            if (csprojFiles.Any() == false)
            {
                var colour = ForegroundColor;
                ForegroundColor = ConsoleColor.Red;
                WriteLine("WARN: No project files found!");
                ForegroundColor = colour;
                return false;
            }
            else
                return true;
        }

        private static void SetVersion(string version, string csprojFile)
        {
            var document = XDocument.Load(csprojFile);
            var projectNode = document.GetOrCreateElement("Project");

            var versionNode = projectNode
                .Elements("PropertyGroup")
                .SelectMany(it => it.Elements("Version"))
                .SingleOrDefault();

            if (versionNode == null)
            {
                versionNode = projectNode
                    .GetOrCreateElement("PropertyGroup")
                    .GetOrCreateElement("Version");
            }

            versionNode.SetValue(version);

            File.WriteAllText(csprojFile, document.ToString());
        }

        private static void PrintSuccessString(string version, IEnumerable<string> files)
        {
            WriteLine($"Set version to {version} in:");

            foreach (var file in files)
                WriteLine($"\t> {file}");

        }
    }
}
