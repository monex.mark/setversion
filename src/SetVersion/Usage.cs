﻿using static System.Console;
namespace SetVersion
{
    internal static class Usage
    {
        internal static void PrintUsage()
        {
            WriteLine("Usage   : SetVersion [versionfile] [solution File] ([additional nuspec file])");
            WriteLine("Example : SetVersion ./src/versionfile ./src/Floodgate.sln");
            WriteLine("\t\t\t: SetVersion ./src/versionfile ./src/Floodgate.sln ./src/TradeStation.SDK.Floodgate.nuspec");
        }

        internal static void PrintUsageValidate()
        {
            WriteLine("Usage   : SetVersion --validate [versionfile] [packages directory]");
            WriteLine("Example : SetVersion --validate ../src/versionfile ../pack");
        }
    }
}
