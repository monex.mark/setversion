﻿using System.IO;
using System.Linq;


namespace SetVersion
{
    internal static class VersionFileReader
    {
        internal static string GetVersionString(string versionFile)
            => File.ReadAllLines(versionFile)
                .Single(l => l.StartsWith("nuget:"))
                .Replace("nuget:", "");
    }
}
