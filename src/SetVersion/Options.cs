﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SetVersion
{
    internal class Options
    {
        internal const string VALIDATE_FLAG = "--validate";

        public bool IsValidate { get; }
        public string PackagesDirectory { get; }

        public string VersionFileLocation { get; }
        public string SolutionFileLocation { get; }
        public string ExtraNuspecFileLocation { get; }
        public bool HasExtraNuget => !string.IsNullOrEmpty(ExtraNuspecFileLocation);
        public string SolutionRoot { get; }

        private Options(string versionFileLocation, string solutionFileLocation, string extraNuspecFileLocation)
        {
            VersionFileLocation = versionFileLocation;
            SolutionFileLocation = solutionFileLocation;
            ExtraNuspecFileLocation = extraNuspecFileLocation;
            SolutionRoot = Path.GetDirectoryName(SolutionFileLocation);
            IsValidate = false;
        }

        private Options(string versionFileLocation, string packagesDirectory)
        {
            VersionFileLocation = versionFileLocation;
            PackagesDirectory = packagesDirectory;
            IsValidate = true;
        }

        internal static (bool success, Options options) TryCreate(IReadOnlyList<string> args)
        {
            if (args == null) throw new ArgumentNullException(nameof(args));
            if (args[0] == VALIDATE_FLAG)
                return TryCreateValidate(args);
            else
                return TryCreateSetValue(args);
        }

        private static (bool success, Options options) TryCreateValidate(IReadOnlyList<string> args)
        {
            if (File.Exists(args[1]) == false || Directory.Exists(args[2]) == false)
                return (false, null);

            var options = new Options(versionFileLocation: args[1], packagesDirectory: args[2]);
            return (true, options);
        }

        private static (bool success, Options options) TryCreateSetValue(IReadOnlyList<string> args)
        {
            if (File.Exists(args[0]) == false || File.Exists(args[1]) == false || File.Exists(args[2]) == false)
                return (false, null);

            var options = new Options(versionFileLocation: args[0], solutionFileLocation: args[1], extraNuspecFileLocation: args[2]);
            return (true, options);
        }
    }
}