﻿using System;
using System.Collections.Generic;
using System.Linq;
using ByteDev.DotNet.Solution;

namespace SetVersion
{
    internal static class SolutionParser
    {
        internal static IReadOnlyList<DotNetSolutionProject> GetAllCsprojFiles(string solutionFilePathAndName, Predicate<string> filenameExcluder)
        {
            var dotNetSolution = DotNetSolution.Load(solutionFilePathAndName);
            return dotNetSolution.Projects
                .Where(p => p.Type.IsSolutionFolder == false)
                .Where(p => !filenameExcluder(p.Name))
                .ToList();
        }

    }
}
